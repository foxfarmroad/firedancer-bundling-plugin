package firedancer.plugin.bundling

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.bundling.Zip

class FiredancerBundlingPlugin implements Plugin<Project> {

    static final CORE_PLUGIN_IDS = ['java']

    static final PLUGIN_IDS = CORE_PLUGIN_IDS

    @Override
    void apply(Project project) {
        project.with {
            PLUGIN_IDS.each { plugins.apply(it) }

            project.task(type: Zip, 'serverlessZip') {
                setGroup('Serverless')
                setDescription('Produces a zip that can be deployed using the Serverless framework')

                subprojects.findAll { Project sub -> sub.name == 'aws-lambda' }.each { Project sub ->
                    PLUGIN_IDS.each { sub.plugins.apply(it) }

                    from sub.getTasksByName('compileJava', false).first()
                    from sub.getTasksByName('processResources', false).first()
                    into('lib') {
                        from sub.getConfigurations().getByName('compile')
                    }
                }
                setBaseName(project.getName())
                setVersion('')
            }

            build.dependsOn serverlessZip
        }
    }
}