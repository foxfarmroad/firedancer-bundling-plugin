package firedancer.plugin.bundling

import nebula.test.PluginProjectSpec

class FiredancerBundlingPluginSpec extends PluginProjectSpec {

    @Override
    String getPluginName() { return 'firedancer.bundling-plugin' }

}